"use strict";

module.exports = function(shipit) {
    require("shipit-db")(shipit);
    var projectConfig = require('./project.config');

    //clone config as we are going to modify it
    var config = JSON.parse(JSON.stringify(projectConfig));
    //define project's default settings
    var shipitConfig = { default: config.paths };
    //set shipit-db config
    shipitConfig = shipit_db(config, shipitConfig);
    //inti shipit config
    shipit.initConfig(shipitConfig);

    /** ########################
     * Deploy files to server
     ########################## */
    shipit.task("deploy", function() {
        var source = shipit.config.source;
        var dest = shipit.config.deployTo;

        var options = shipit.config;
        options.rsync = ["-avz"]; //recursion , vervose , compressed

        return shipit.remoteCopy(source, dest, options);
    });


    /** ##############################
     * Automatically generates a wp-config-env.php file to swith enviroments in wp-config.php
     * this task can be called with any invironment
     ############################## */
    shipit.task("wp_config", function() {
        //check the generated file name is in the config
        var targetFile = shipit.config.wp_config;
        if (!targetFile) {
            throw Error("Error:path for generated file missing, set the file path in project.config.js variable: 'paths.wp_config'");
        }

        const fs = require("fs");
        var Handlebars = require("handlebars");
        var source = fs.readFileSync("tasks/wp-config-env-template.php", "utf8");
        var template = Handlebars.compile(source);
        var data = projectConfig;
        var result = template(data);
        var res = fs.writeFileSync(targetFile, result, "utf8");
        console.log("WP config file created in: " + targetFile);
        console.log("To use this configuration add 'include_once(" + targetFile + ")' to wp-config.php and remove any previous configuration ");
    });


};


/**
 * Parses the config file and initalizes the config file as requierred by shipit-db
 * see: https://github.com/timkelty/shipit-db
 */
function shipit_db(projectConfig, shipitConfig) {
    //sets the shipit-db
    var envs = projectConfig.environments;
	var util = require('util');
    Object.keys(envs).forEach(function(key) {
        var env = envs[key];
        if (key == 'local' || key == 'development' || key == 'default' ) {
            //check parameters
            if (!env.db || !env.host_names) {
                throw Error("Error: 'db' or 'host_names' parameter missing in environment '" + key + "'");
            }
            var db = env.db;
            env.db = null;
            shipitConfig.default.db = {
                local: db
            };
        } else {
            //check parameters
            if (!env.db || !env.host_names || !env.deployTo || !env.servers) {
                throw Error("Error: 'db', 'host_names', 'deployTo' or 'servers' parameter missing in environment '" + key + "'");
            }
            //set the db as remote
            var db = env.db;
            env.db = null;
            shipitConfig[key] = env;
            shipitConfig[key].db = {
                remote: db
            }
        }
    });

    return shipitConfig;
}
